<div align="center">
    <h1>indian-postal-codes</h1>
</div>

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.0.

## Table of contents
1. [Getting Started](#getting-started)
2. [Demo](#demo)
3. [Installation](#installation)
4. [Usage](#usage)

## Getting Started

indian-postal-codes provides powered by Angular, so you don't need to include original JS components. 

## Demo

Indian Postal Codes components for Angular applications, demos and API documentation could be found here:
[http://thaaimozhikalvi.com/demo/pincode](http://thaaimozhikalvi.com/demo/pincode).

## Installation

Install `indian-postal-codes` from `npm`:
```bash
npm install indian-postal-codes --save
```

## Usage
Add wanted package to NgModule imports:
```typescript
import {IndianPostalCodesModule} from "indian-postal-codes";

@NgModule({
  ...
  imports: [
        ...,
        IndianPostalCodesModule,
        ...]
  ...
})
```

Add component to your page:
```typescript
import {IndianPostalCodesService} from "indian-postal-codes";

@Component({...})

export class AppComponent {
  constructor(private postalCodesService: IndianPostalCodesService) {}
}
```

Find By PinCodes:

```typescript
this.postalCodesService.findByPinCode(pinCode).subscribe(data => {
      if (data[0].Status === 'Success') {
        this.result = data[0].PostOffice;
      } else if (data[0].Status === 'Error') {
        alert(data[0].Message);
        this.result = [];
      } else if (data[0].Status === '404') {
        alert(data[0].Message);
        this.result = [];
      }
    });
```

Find By Post Office:
```typescript
this.postalCodesService.findByPostOfficeName(postOffice).subscribe(data => {
      if (data[0].Status === 'Success') {
        this.result = data[0].PostOffice;
      } else if (data[0].Status === 'Error') {
        alert(data[0].Message);
        this.result = [];
      } else if (data[0].Status === '404') {
        alert(data[0].Message);
        this.result = [];
      }
    });
```
## Creator

#### [GP DHANUSH](mailto:agprakash406@gmail.com)

- [@GitLab](https://gitlab.com/gpdhanush)
