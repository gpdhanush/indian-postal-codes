/*
 * Public API Surface of indian-postal-codes
 */

export * from './lib/indian-postal-codes.service';
export * from './lib/indian-postal-codes.component';
export * from './lib/indian-postal-codes.module';
