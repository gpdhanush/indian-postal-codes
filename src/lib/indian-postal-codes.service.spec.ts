import { TestBed } from '@angular/core/testing';

import { IndianPostalCodesService } from './indian-postal-codes.service';

describe('IndianPostalCodesService', () => {
  let service: IndianPostalCodesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IndianPostalCodesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
