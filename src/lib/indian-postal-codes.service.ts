import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
export interface PostOfficeDetails {
  Name: String,
  Description: String,
  BranchType: String,
  DeliveryStatus: String,
  Circle: String,
  District: String,
  Division: String,
  Region: String,
  State: String,
  Country: String,
  Pincode: String,
}

@Injectable({
  providedIn: 'root'
})
export class IndianPostalCodesService {

  constructor(private http: HttpClient) { }
  findByPinCode(pinCode: any): Observable<any> {
    return this.http.get<any>('https://api.postalpincode.in/pincode/' + pinCode).pipe(map((data: PostOfficeDetails) => {
        return data
      })
    );
  }
  findByPostOfficeName(postOffice: any): Observable<any> {
    return this.http.get<any>('https://api.postalpincode.in/postoffice/' + postOffice).pipe(map((data: PostOfficeDetails) => {
        return data
      })
    );
  }
}
