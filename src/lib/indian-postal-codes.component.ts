import { Component, OnInit } from '@angular/core';
import {IndianPostalCodesService} from "./indian-postal-codes.service";
@Component({
  selector: 'lib-indian-postal-codes',
  template: ``
})
export class IndianPostalCodesComponent implements OnInit {
  constructor(private postalCodesService: IndianPostalCodesService) { }

  ngOnInit(): void {
  }
}
