import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndianPostalCodesComponent } from './indian-postal-codes.component';

describe('IndianPostalCodesComponent', () => {
  let component: IndianPostalCodesComponent;
  let fixture: ComponentFixture<IndianPostalCodesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndianPostalCodesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndianPostalCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
