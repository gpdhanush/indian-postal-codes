import { NgModule } from '@angular/core';
import { IndianPostalCodesComponent } from './indian-postal-codes.component';



@NgModule({
  declarations: [
    IndianPostalCodesComponent
  ],
  imports: [
  ],
  exports: [
    IndianPostalCodesComponent
  ]
})
export class IndianPostalCodesModule { }
